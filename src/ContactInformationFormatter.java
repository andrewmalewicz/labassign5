import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ContactInformationFormatter implements IContactInformationFormatter {


	private Scanner scan;
	private FormatExceptionHandler form = new FormatExceptionHandler();
	
	@Override
	public void readContactInformation(String[] filePaths) {	 
		
		for(int x = 0; x < filePaths.length; x++) {		
			try {
				scan = new Scanner(new File(filePaths[x]));
				formatContactInformation(filePaths[x]);

						while(scan.hasNextLine()) {
							
							try {
							formatName(scan.nextLine());
							}
							catch(NameFormatException e) {
								form.handleNameFormatException(e);
							}
							
							try {
							formatPhoneNumber(scan.nextLine());
							}
							catch(PhoneNumberFormatException e) {
								form.handlePhoneNumberFormatException(e);
							}
							
							try {
							formatEmail(scan.nextLine());
							}
							catch(EmailAddressFormatException e) {
								form.handleEmailFormatException(e);
							}
						}

			} catch (FileNotFoundException e) {
				form.handleFileNotFoundException(e);
			}		
		}
		
	}

	@Override
	public void formatContactInformation(String fileName) {
		
		//System.out.println(this.fileName);
	}

	@Override
	public void formatEmail(String email) throws EmailAddressFormatException {
		
		try {
			if(Pattern.matches("[a-z]([a-z])*(.[a-z]+)*@[a-z]+.(a-z)+", email)) {
				System.out.println(email);
			}
			else {
				throw new EmailAddressFormatException(email);
			}
		}
		catch(EmailAddressFormatException e) {
			
		}
	}

	@Override
	public void formatPhoneNumber(String phoneNumber) throws PhoneNumberFormatException {
		
		try {
			if(Pattern.matches("([0-9][0-9][0-9])-[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]", phoneNumber)) {
				System.out.println(phoneNumber);
			}
			else {
			throw new PhoneNumberFormatException(phoneNumber);
			}
		}
		catch(PhoneNumberFormatException e) {
			
		}

	}

	@Override
	public void formatName(String name) throws NameFormatException {
		
		try {
			if(Pattern.matches("[A-Z][a-z]+[ ][A-Z][a-z]+", name)) {
				System.out.println(name);
			}
			else {
			throw new NameFormatException(name);
			}
		}
		catch(NameFormatException e) {
			
		}

	}



}
