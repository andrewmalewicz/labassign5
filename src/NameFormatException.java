
public class NameFormatException extends Exception {
	
	public NameFormatException() {
		
	}
	
	public NameFormatException(String name) {
		String changeCase = name.replaceFirst("[A-Z][a-z]+[ ][A-Z][a-z]+", name);
		System.out.println(changeCase + " works");
	}
}
