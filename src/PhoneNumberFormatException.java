
public class PhoneNumberFormatException extends Exception {
	
	public PhoneNumberFormatException(String phoneNumber) {
		String input = phoneNumber;
		input = input.replace("(","");
		input = input.replace(")","");
		input = input.replace("-","");
		String changeNum = input.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1)-$2-$3");
		System.out.println(changeNum);
	}
	
}
